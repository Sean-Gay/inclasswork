 
********COMPILING CLASS VERSION************
g++ -g -std=gnu++11 -Wpedantic testDateCla.cpp dateCla.cpp -o testDateCla			
 
********COMPILING BOTH VERSIONS WITH make all************
g++ -g -std=gnu++11 -Wpedantic testDateStr.cpp -o testDateStr 			
g++ -g -std=gnu++11 -Wpedantic testDateCla.cpp dateCla.cpp -o testDateCla			
 
***************Running testDateStr...******************
The check date 2019 JAN 18 is before the initial date 2019 JAN 23
The check date 2019 JAN 23 is the same day or after the initial date 2019 JAN 23
The check date 2019 JAN 25 is the same day or after the initial date 2019 JAN 23
 
***************Running testDataCla...******************
The check date 2021 FEB 17 is before the initial date 2021 FEB 22
The check date 2021 FEB 22 is the same day or after the initial date 2021 FEB 22
The check date 2021 FEB 26 is the same day or after the initial date 2021 FEB 22
 
********CLEAN STRUCT VERSION************
rm -f testDateStr *.o *.out
 
********CLEAN CLASS VERSION************
rm -f testDateCla *.o *.out
 
********CLEANEST VERSION************
rm -f testDateStr *.o *.out
rm -f testDateCla *.o *.out
allCommandsRun.txt
allCommands.sh
dateCla.cpp
dateCla.h
dateStr.h
mainCommands.sh
Makefile
README
testDateCla.cpp
testDateStr.cpp
testDateStr.h
